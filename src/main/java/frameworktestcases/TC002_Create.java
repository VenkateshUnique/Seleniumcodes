package frameworktestcases;



import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import Week6day2.LearningToReadExcel;
import methodarchitect.ProjectMethods;


public class TC002_Create extends ProjectMethods
{
	@BeforeClass//(groups="Common")
	
	public void setData()
	{
		 testcaseName = "TC002_CreateLead ";
		 testcaseDescription = "Testcase to create lead" ; 
		 testcaseAuthor= "Venkatesh";
		testcaseCategory = "Smoke" ;
		excelfilename = "CreateLead";
	}
		
	@Test(/*groups="Smoke",*/dataProvider = "fetchdata")//, //invocationCount = 2 , //invocationTimeOut = 3000)
	public void createLead(String cmpname,String fname,String lname,String flocname,String phcode)
	{
		WebElement element5 = locateElement("linktext", "Leads");
		click(element5);
		WebElement element6 = locateElement("linktext", "Create Lead");
		click(element6);
		WebElement element7 = locateElement("id","createLeadForm_companyName");
		type(element7, cmpname);
		WebElement element8 = locateElement("id","createLeadForm_firstName");
		type(element8, fname);
		WebElement element9 = locateElement("id","createLeadForm_lastName");
		type(element9, lname);
		WebElement element10 = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(element10, "Employee");
		WebElement element11 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(element11, "Pay Per Click Advertising");
		WebElement element12 = locateElement("id","createLeadForm_firstNameLocal");
		type(element12, flocname);
		WebElement element13 = locateElement("id","createLeadForm_primaryPhoneCountryCode");
		typeandclear(element13, phcode);
		WebElement element14 = locateElement("class","smallSubmit");
		click(element14);	
		WebElement element15= locateElement("id", "viewLead_companyName_sp");
		System.out.println("print here ---> "+ getText(element15));
		closeAllBrowsers();
	}
}
