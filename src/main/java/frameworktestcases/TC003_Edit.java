package frameworktestcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Week6day2.LearningToReadExcel;
import methodarchitect.ProjectMethods;


public class TC003_Edit extends ProjectMethods
{	
	
	@BeforeClass//(groups = "common")
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDescription = "TestCase to Edit Lead";
		testcaseAuthor= "Venkatesh";
		testcaseCategory = "Sanity";
		excelfilename = "EditLead";
		
	}
	
	
	
	@Test (dataProvider = "fetchdata"/*dependsOnMethods="frameworktestcases.TC002_Create.createLead",*/ /*groups = "Sanity",dependsOnGroups= "Smoke"*/)
	public void editLead(String fname,String cmpname)
	{
		//login();
		
		WebElement ele5 = locateElement("linktext", "Lead");
		click(ele5);
		WebElement ele6 = locateElement("linktext", "Find Leads");
		click(ele6);
		WebElement ele7 = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(ele7,fname);
		WebElement ele8 = locateElement("xpath", "//button[text()='Find Leads']");
		click(ele8);
		WebElement ele9 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(ele9);
		verifyTitle("View Lead | opentaps CRM");
		WebElement ele10 = locateElement("linktext","Edit");
		click(ele10);
		WebElement ele11 = locateElement("id","updateLeadForm_companyName");
		typeandclear(ele11,cmpname);
		WebElement ele12 = locateElement("xpath","//input[@name='submitButton'][1]");
		click(ele12);
		//closeBrowser();
	}

	}


