package Week6day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearningToReadExcel {
	public static Object[][] getExcelData (String filename) throws IOException {

		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		//get the last row
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("The last row number is "+lastRowNum);
		//to find the last column in the row
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		System.out.println("The last cell number is "+lastCellNum);

		Object [][]data = new Object[lastRowNum][lastCellNum];

		for (int j = 1; j <=lastRowNum; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastCellNum; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue(); // may be considered to change as getStringvalue
				data [j-1][i] = stringCellValue;
			} 
		}
		wbook.close();
		return data;
	}

}	
