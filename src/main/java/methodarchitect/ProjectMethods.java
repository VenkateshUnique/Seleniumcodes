package methodarchitect;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Week6day2.LearningToReadExcel;


public class ProjectMethods extends SeMethods
{
	@DataProvider (name = "fetchdata")
	public Object[][] readData() throws IOException
	{
		Object[][] exceldata = LearningToReadExcel.getExcelData(excelfilename);
		return exceldata;
	}
	
	@Parameters ({"Webbrowser","Appurl","Username","Password"})
	@BeforeMethod//(groups="Common")
	public void login(String webbrow,String url,String uname,String pwd) 
	{
		startTestCase();
		startApp(webbrow, url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement ele4 = locateElement("linktext", "CRM/SFA");
		click(ele4);
	}
	@AfterMethod//(groups="Common")
	public void close()
	{
		closeAllBrowsers();
	}
}
