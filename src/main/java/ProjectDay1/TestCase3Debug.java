package ProjectDay1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;


public class TestCase3Debug {

	@Test
	public void debug() throws InterruptedException  {
	
		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeOptions op = new ChromeOptions();
		op.addArguments("Disable-notifications");
		ChromeDriver driver = new ChromeDriver(op);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.myntra.com/");
		
		// Mouse Over on Men
		driver.findElementByLinkText("Men");
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByLinkText("Jackets").click();
		
		// Find the count of Jackets
		WebElement leftcount = driver.findElementByXPath("//input[@value='Jackets']/following::span[1]");
		String Lvalue = leftcount.getText();
		System.out.println(Lvalue);
		
		// Click on Jackets and confirm the count is same
		WebElement chbx = driver.findElementByXPath("//div[@class='common-checkboxIndicator']");
		chbx.click();
		
		// Wait for some time
		Thread.sleep(3000);

		// Check the count
		WebElement rightcount = driver.findElementByXPath("//span[@class='horizontal-filters-sub']");
		String Rvalue = rightcount.getText();
		System.out.println(Rvalue);

		// If both count matches, say success
		if(Lvalue.contains(Rvalue))
		{
			System.out.println("Success the count matches ");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPriceEle = driver.findElementsByXPath("//div[@class='product-price']");
		List<String> listPrice = new ArrayList<String>();
		for (WebElement eachpriceitem : productsPriceEle) {
			System.out.println(eachpriceitem.getText());
			listPrice.add(eachpriceitem.getText());
		}
		System.out.println(listPrice.size());
		
		Collections.sort(listPrice);
		String lastitem = listPrice.get(0);
		System.out.println(lastitem);
		
		// Sort them 
		//String max = Collections.max(lastitem);

		// Find the top one
		//System.out.println(max);
		
		//Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//div[@class='brand-more']").click();
		

		/*// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("//D", ""));
		}

		// Get the minimum Price 
		String min = Collections.min(onlyPrice);

		// Find the lowest priced Allen Solly
		System.out.println(min);


*/	}

}


